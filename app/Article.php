<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";
    protected $fillable = ['name','branch','model','price','user_id'];

    public function user(){
      return $this->belongsTo('App\User');
    }
    public function orders(){
      return $this->hasMany('App\Order');
    }
    public function images(){
      return $this->hasMany('App\Image');
    }
}
